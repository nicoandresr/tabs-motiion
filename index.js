/**
 * version 0.1 - beta version
 */

browser.commands.onCommand.addListener(async function () {
  // get current tab index
  console.info('test');
  const { index: currentTabIndex } = await browser.tabs.getCurrent();
  const tabs = await browser.tabs.query({ currentWindow: true, hidden: false });

  // Go to the first tab
  if (!tabs[currentTabIndex + 1]) {
    browser.tabs.update(tabs[0].id, { active: true });
    return;
  }

  // Otherwise go to the next tab
  browser.tabs.update(tabs[currentTabIndex + 1].id, { active: true });
});
