# Tabs Motiion
This extension provides the easy way to move throgh tabs on Firefox with Ctrl+Number, this project is inspired by my custom keyboard layout where I changed the BlockNum key by the Ctrl key, and for me is easy use the Crtl + N to move throgh tabs.
## Installation
This extension can be installed from the Firefox Add-ons site.
## Credits:
I based my initial version on the https://github.com/karakays/firefox-ctrlnumber addon by karakays.
